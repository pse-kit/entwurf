\subsection{Mailer}
Das Modul Mailer versendet E-Mail-Benachrichtigungen an \gls{Nutzer} und \glspl{Administrator}. Mithilfe der Erweiterung \textit{Celery} können E-Mails asynchron im Hintergrund versendet werden. \\
Es gibt folgende E-Mail-Methoden:
\vspace{-4mm}
\begin{argosbox}{Auftragsbestätigung}
  Hat ein Nutzer einen Auftrag erstellt, so erhält er eine Bestätigungs-E-Mail mit allen Auftragsdetails.
  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|send_confirmation_mail(task: Task): void|
    \item[Beschreibung] Die Methode sendet eine E-Mail an den Ersteller des Auftrags \verb|task| unter Verwendung eines vorgefertigten Templates.
  \end{description}
\end{argosbox}

\begin{argosbox}{Zu signierender Auftrag}
  Steht ein neuer Auftrag zur Verfügung, den ein Nutzer signieren kann, erhält er eine Benachrichtigungs-E-Mail.
  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|send_tosign_mail(task: Task): void|
    \item[Beschreibung] Die Methode sendet eine E-Mail an alle Nutzer, die den Auftrag \verb|task| signieren können. Hierbei wird ein vorgefertigtes Template verwendet.
  \end{description}
\end{argosbox}

\begin{argosbox}{Abgeschlossener Auftrag}
  Wurde ein Auftrag abgeschlossen, so erhält der Auftragsersteller eine Benachrichtigungs-E-Mail.
  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|send_finalize_mail(task: Task): void|
    \item[Beschreibung] Die Methode sendet eine E-Mail an den Ersteller des Auftrags \verb|task| unter Verwendung eines vorgefertigten Templates.
  \end{description}
\end{argosbox}

\begin{argosbox}{Abgelehnter Auftrag}
  Wurde ein Auftrag abgelehnt, so erhält der Auftragsersteller eine Benachrichtigungs-E-Mail.
  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|send_decline_mail(task: Task): void|
    \item[Beschreibung] Die Methode sendet eine E-Mail an den Ersteller des Auftrags \verb|task| unter Verwendung eines vorgefertigten Templates.
  \end{description}
\end{argosbox}

\begin{argosbox}{Allgemeine E-Mail-Methode}
  Diese Methode wird von allen anderen Methoden dieses Moduls mit den entsprechenden Argumenten aufgerufen.
  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \texttt{send_mail(to: String, subject: String, template: String,}\\ \texttt{**kwargs: Dictionary): void}
    \item[Beschreibung] Die Methode sendet eine E-Mail an die E-Mail-Adresse \verb|to| mit dem Betreff \verb|subject|. Dabei wird das vorgefertigte Template \verb|template| mit den Parametern in \verb|**kwargs| verwendet.
  \end{description}
\end{argosbox}

\subsection{Synchronizer} 
Das Modul Synchronizer aktualisiert in bestimmten Zeitabständen die Nutzer und ihre \glslink{Gruppe}{Gruppenmitgliedschaften} in der Datenbank. Dabei werden lediglich neue Nutzer und neue Gruppenmitgliedschaften hinzugefügt, jedoch keine solchen entfernt. Die Zuordung von Nutzern aus dem \gls{LDAP} zu Nutzern aus der Datenbank erfolgt hierbei über den Nutzernamen aus dem LDAP.
Damit das Modul Synchronizer den \gls{Webdienst} nicht blockiert, wird die Erweiterung \textit{Celery} verwendet, wodurch der Synchronizer im Hintergrund arbeiten kann.
\begin{description}
  \item[Methodenaufruf] \verb|synchronize(): void|
  \item[Beschreibung] Die Methode aktualisiert die Datenbank wie oben beschrieben.
\end{description}

\subsection{Authenticator} 
Der Authenticator ist für die Nutzer- und Administrator-Authentifizierung zuständig. \name verwendet zur Überprüfung, ob ein Nutzer beziehungsweise ein Administrator angemeldet ist, \textit{JSON Web Token}. Mit gültigen Login-Daten erhält der Anfragende ein \textit{Access-Token} mit einer relativ kurzen Lebensdauer von 15 Minuten sowie ein \textit{Refresh-Token}, welches 24 Stunden gültig ist und die Anforderung neuer \textit{Access-Token} erlaubt. Das \textit{Access-Token} muss für alle weiteren \glslink{REST-API}{API}-Anfragen außerhalb des Authenticator-Moduls als Zugriffsschutz und zur Identifizierung des Anfragenden im HTTP-Header wie folgt mitgeschickt werden: \texttt{Authorization: Bearer <access_token>}. \newline
Folgende Flask-Endpunkte werden vom Authenticator-Modul bereitgestellt:
\vspace{-5mm}
\begin{routesbox}{\fbox{POST} \textbf{/api/v1/login}\textcolor{darkgray}{\textbf{?\textit{role=user}}}}
  \quad
  \vspace{-5mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|role| (optional) & String & Die Rolle des Anfragenden. Mögliche Werte: \newline \verb|user|: Nutzer (default)\newline \verb|admin|: Administrator\\
    \hline
  \end{longtabu*}

  Der JSON-String, der dieser Route übergeben wird, hat folgendes Format:

  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Attribut & Typ & Beschreibung \\
    \hline
    \verb|username| & String & Bei Nutzern: LDAP-Username (\texttt{vorname.nachname[Zahl]}) \newline Bei Administratoren: Admin-Username (z.B.: \texttt{admin}) \\
    \hline
    \verb|password| & String & Passwort\\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|login(): Response|
    \item[Beschreibung] \hfill \\
    Die Methode überprüft die übermittelten Login-Daten (bei Nutzern durch eine LDAP-Abfrage und bei Administratoren durch eine Datenbank-Abfrage), generiert ein neues \textit{Access-Token} beziehungsweise \textit{Refresh-Token} und speichert diese in der Datenbank. Anschließend werden die Tokens zurückgegeben.
    \item[Mögliche Fehler] \hfill \\ 
    \textit{Unauthorized}: Der Anfragende hat ungültige Login-Daten übermittelt.
    \item[Rückgabe] Statuscode, JSON-String mit \textit{Access-Token} und \textit{Refresh-Token}
  \end{description}

\end{routesbox}  

\begin{routesbox}{\fbox{POST} \textbf{/api/v1/refresh}}

  Im Authorization-Header der HTTP-Anfrage wird ein Argument erwartet:
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|refresh_token| & String & \textit{JSON-Token-Identifier}. Zeichenfolge, welches das \textit{Refresh-Token} eindeutig identifiziert. \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|refresh(): Response|
    \item[Beschreibung] \hfill \\
    Die Methode überprüft das übermittelte \textit{Refresh-Token} auf Gültigkeit, erzeugt gegebenenfalls ein neues \textit{Access-Token}, speichert es in der Datenbank und gibt es zurück.
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende hat ein ungültiges \textit{Refresh-Token} übermittelt.
    \item[Rückgabe] Statuscode, JSON-String mit \textit{Access-Token}
  \end{description}

\end{routesbox}

\begin{routesbox}{\fbox{POST} \textbf{/api/v1/logout}}

  Im Authorization-Header der HTTP-Anfrage wird ein Argument erwartet:
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|jti| & String & \textit{JSON-Token-Identifier}. Zeichenfolge, welches das \textit{Access-Token} oder \textit{Refresh-Token} eindeutig identifiziert. \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|revoke(): Response|
    \item[Beschreibung] Die Methode setzt den \texttt{revoked_status} des \textit{Tokens} in der Datenbank auf \texttt{true}, somit kann es nicht mehr zur Authentifizierung genutzt werden.
    \item[Mögliche Fehler] \hfill \\
    \textit{Bad Request}: Der Anfragende hat ein bereits invalidiertes oder ein nicht ausgestelltes \textit{Token} übermittelt.
    \item[Rückgabe] Statuscode
  \end{description}

\end{routesbox}

\newpage
\subsection{Error}
Das Modul Error beschreibt mögliche auftretende Fehler und die entsprechenden Statuscodes.
\begin{description}
  \item[Bad Request] \hfill \\
  \textbf{Statuscode}: 400 \\
  \textbf{Ursache}: Der in der Anfrage übergebene JSON-String entspricht nicht den Erwartungen. Attribute haben falsche Datentypen, notwendige Attribute sind nicht vorhanden oder es wird eine Identifikationsnummer referenziert, die nicht existiert.
  \item[Unauthorized] \hfill \\
  \textbf{Statuscode}: 401 \\
  \textbf{Ursache}: Der Anfragende ist nicht angemeldet oder hat ungültige Login-Daten übermittelt, muss aber für die Anfrage authentifziert sein.
  \item[Forbidden] \hfill \\
  \textbf{Statuscode}: 403 \\
  \textbf{Ursache}: Der anfragende Nutzer oder Administrator darf nicht auf die angefragte Ressource zugreifen.
  \item[Not Found] \hfill \\
  \textbf{Statuscode}: 404 \\
  \textbf{Ursache}: Die in der URL angegebenen Identifikationsnummern existieren nicht und die angeforderte Ressource konnte nicht gefunden werden.
\end{description}
Bei einem Fehler wird ein JSON-String, das die Fehlerart und eine Fehlermeldung beinhaltet, in folgendem Schema zurückgegeben:

\lstinputlisting[language=json, frame=none]{./Webdienst/Schema/error.json}

\newpage
\subsection{Utils}
Das Modul Utils stellt Kommandozeilen-Befehle zur Verfügung, um einen Administrator hinzuzufügen und abgelaufene Tokens aus der Datenbank zu löschen.

\begin{argosbox}{Admin hinzufügen}
  \begin{description}[leftmargin=0pt]
    \item[Kommandozeilen-Eingabe] \texttt{flask add_admin <first_name> <last_name>}\\ \texttt{<mail_adress> <username> <password>}
    \item[Methodenaufruf] \texttt{add_admin(first_name: String, last_name: String,}\\ \verb|mail_adress: String, username: String, password: String): void|
    \item[Beschreibung] Die Methode erstellt einen neuen Administrator mit den übergebenen Parametern und speichert diesen in der Datenbank.  
  \end{description}
\end{argosbox}

\begin{argosbox}{Abgelaufene Tokens aus Datenbank entfernen}
  \begin{description}[leftmargin=0pt]
    \item[Kommandozeilen-Eingabe] \texttt{flask clear_revoked_tokens}
    \item[Methodenaufruf] \verb|clear_revoked_tokens(): void| 
    \item[Beschreibung] Die Methode löscht alle abgelaufenen Tokens aus der Datenbank. 
  \end{description}
\end{argosbox}