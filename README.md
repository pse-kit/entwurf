# Entwurf
Entwurfsdokument des PSE-Projekts "Webservice zur Definition und Durchsetzung des Mehr-Augen-Prinzips" im WS2018/19 von Julius Häcker, Moritz Leitner, Nicolas Schuler, Noah Wahl und Wendy Yi

### PDF
Entwurfsdokument:
```console
pdflatex Entwurf.tex
biber Entwurf
pdflatex Entwurf.tex
```

Präsentation: `pdflatex Presentation/Presentation.tex `


