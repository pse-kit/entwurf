\subsection{Routes}
Das Modul Routes spezifiziert die \gls{REST-API} von \name und bildet die Schnittstelle zum Client. Damit verbirgt es die anderen Module und insbesondere die Datenbank des \glslink{Webdienst}{Webdiensts}.
 Die verschiedenen Routen sind, abhängig davon, welche Ressource sie identifizieren, in die Module \texttt{endpoint_handler}, \texttt{task_handler}, \texttt{user_handler}, \texttt{message_handler}, \texttt{preferences}\verb|_handler| und \texttt{group_handler} aufgeteilt.
Diese Routen sind jeweils an Methoden geknüpft.

\newpage
\subsubsection{endpoint_handler}
\begin{routesbox}{\fbox{GET} \textbf{/api/v1/endpoints/\textit{<endpoint_id>}}}

  \quad
  \vspace{-5mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|endpoint_id| & int & Die eindeutige Identifikationsnummer des Endpunkts. \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|get_endpoint(endpoint_id: int): Response|
    \item[Beschreibung] Die Methode gibt die Details eines Endpunkts mit der Identifikationsnummer \verb|endpoint_id| zurück.
    \item[Nötige Zugriffsrechte] \gls{Administrator} oder \gls{Nutzer}
    \item[Mögliche Fehler] \hfill \\
        \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
        \textit{Not Found}: Kein aktiver Endpunkt zur gegebenen Identifikationsnummer.
    \item[Rückgabe] Statuscode, JSON-String mit einem Endpoint-Objekt im \nameref{sec:endpoint}{-Schema}
  \end{description}

\end{routesbox}

\begin{routesbox}{\fbox{GET} \textbf{/api/v1/endpoints}\textcolor{darkgray}{\textbf{?\textit{limit=20}\&\textit{page=1}}}}

  \quad
  \vspace{-5mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|limit| (optional) & int & Die maximale Anzahl an Endpunkten (default: 20), die zurückgegeben wird.\\
    \hline
    \verb|page| (optional) & int & Die angefragte Seite (default: 1). \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|get_endpoints_active(): Response|
    \item[Beschreibung] Die Methode gibt die Details zu allen aktiven Endpunkte zurück.
    \item[Nötige Zugriffsrechte] Administrator oder Nutzer
    \item[Mögliche Fehler] \hfill \\
      \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
      \textit{Bad Request}: \verb|limit|, \verb|page| keine natürlichen Zahlen
    \item[Rückgabe] Statuscode, JSON-Objekt mit Endpoint-Objekten im \nameref{sec:endpoint}{-Schema}
  \end{description}

\end{routesbox}

\begin{routesbox}{\fbox{POST} \textbf{/api/v1/endpoints}}

  Der JSON-String, der dieser Route übergeben wird, hat folgendes Format:

  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Attribut & Typ & Beschreibung \\
    \hline
    \verb|name| & String & Name des Endpunkts. \\
    \hline
    \verb|description| & String & Beschreibung des Endpunkts. \\
    \hline
    \verb|signature_type| & String & Gibt die Art der Signaturmethode an. \\
    \hline
    \verb|sequential_status| & boolean & Modus des Endpunkts. \newline \verb|True| für sequentiellen Modus\newline \verb|False| für parallelen Modus \\
    \hline
    \verb|deletable_status| & boolean & Löschbarkeit der Aufträge zu diesem Endpunkt. \\
    \hline
    \verb|fields| & Array of \nameref{sec:field}{} & Eine Menge an Feldern, die aus Typ und Name bestehen. \\
    \hline
    \verb|allow_additional| \newline \verb|_accesses_status|  & boolean & Gibt an, ob die benötigten Signaturen vom Auftragsersteller erweitert werden dürfen. \\
    \hline
    \verb|given_accesses| & Array of \nameref{sec:given_access}{} & Eine Menge an \glspl{Gruppe}/Nutzern, die einen Auftrag zu diesem Endpunkt signieren müssen. \\
    \hline
    \verb|email_notification| \newline \verb|_status| & String &  Gibt an, ob ein Nutzer, dessen Signatur benötigt wird, eine E-Mail-Benachrichtigung bekommt.\\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|create_endpoint(): Response|
    \item[Beschreibung] Die Methode erzeugt zu den in einem JSON-String übergebenen Attributen eines Endpunkts einen neuen Endpunkt und fügt diesen zu der Datenbank hinzu.
    \item[Nötige Zugriffsrechte] Administrator
    \item[Mögliche Fehler]  \hfill \\
      \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
      \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
      \textit{Bad Request}: Übergebene Attribute sind ungültig (falscher Datentyp, zu wenig Daten, referenzierte Nutzer/Gruppen existieren nicht).
    \item[Rückgabe] Statuscode, JSON-String mit dem erstellten Endpunkt im \nameref{sec:endpoint}{-Schema} und der URL des erstellten Endpunkts.   
  \end{description}

\end{routesbox}


\begin{routesbox}{\fbox{DELETE} \textbf{/api/v1/endpoints/\textit{<endpoint_id>}}}

  \quad
  \vspace{-5mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|endpoint_id| & int & Die eindeutige Identifikationsnummer des Endpunkts. \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|set_endpoint_deprecated(endpoint_id: int): Response|
    \item[Beschreibung] Die Methode setzt den Status des Endpunkts mit der gegebenen Identifikationsnummer auf nicht instanziierbar. Es können keine neuen Aufträge zu diesem Endpunkt erstellt werden.
    \item[Nötige Zugriffsrechte] Administrator
    \item[Mögliche Fehler]  \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
    \textit{Not found}: Kein aktiver Endpunkt zur gegebenen Identifikationsnummer.
    \item[Rückgabe] Statuscode
  \end{description}

\end{routesbox} 

\subsubsection{task_handler}

\begin{routesbox}{\fbox{GET} \textbf{/api/v1/tasks/\textit{<task_id>}}}

  \quad
  \vspace{-5mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|task_id| & int & Die eindeutige Identifikationsnummer des Auftrags. \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|get_task(task_id: int): Response|
    \item[Beschreibung] Die Methode gibt die Details eines Auftrags mit der Identifikationsnummer \verb|task_id| zurück.
    \item[Nötige Zugriffsrechte] Administrator oder Nutzer, der den Auftrag erstellt oder signiert hat beziehungsweise als nächstes signieren muss
    \item[Mögliche Fehler]  \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
    \textit{Not Found}: Kein aktiver Auftrag zur gegebenen Identifikationsnummer.
    \item[Rückgabe] Statuscode, JSON-String mit einem Task-Objekt im \nameref{sec:task}{-Schema}
  \end{description}

\end{routesbox}

\begin{routesbox}{\fbox{GET} \textbf{/api/v1/tasks}\textcolor{darkgray}{\textbf{?\textit{limit=20}\&\textit{page=1}}}}

  \quad
  \vspace{-5mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|limit| (optional) & int & Die maximale Anzahl an Aufträgen (default: 20), die zurückgegeben wird.\\
    \hline
    \verb|page| (optional) & int & Die angefragte Seite (default: 1). \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|get_tasks_active(task_id: int): Response|
    \item[Beschreibung] Die Methode gibt alle aktiven Aufträge zurück.
    \item[Nötige Zugriffsrechte] Administrator
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
    \textit{Bad Request}: \verb|limit|, \verb|page| keine natürlichen Zahlen.
    \item[Rückgabe] Statuscode, JSON-String mit Task-Objekten im \nameref{sec:task}{-Schema}
  \end{description}

\end{routesbox}

\begin{routesbox}{\fbox{GET} \textbf{/api/v1/tasks?type=OWNED}\textcolor{darkgray}{\textbf{?\textit{limit=20}\&\textit{page=1}}}}

  \quad
  \vspace{-5mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|limit| (optional) & int & Die maximale Anzahl an Aufträgen (default: 20), die zurückgegeben wird.\\
    \hline
    \verb|page| (optional) & int & Die angefragte Seite (default: 1). \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|get_tasks_owned(): Response|
    \item[Beschreibung] Die Methode gibt alle aktiven Aufträge, die vom anfragenden Nutzer erstellt wurden, zurück.
    \item[Nötige Zugriffsrechte] Nutzer
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
      \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
      \textit{Bad Request}: \verb|limit|, \verb|page| keine natürlichen Zahlen.
    \item[Rückgabe] Statuscode, JSON-String mit Task-Objekten im \nameref{sec:task}{-Schema}
  \end{description}

\end{routesbox}

\begin{routesbox}{\fbox{GET} \textbf{/api/v1/tasks?type=TOSIGN}\textcolor{darkgray}{\textbf{?\textit{limit=20}\&\textit{page=1}}}}
  \quad
  \vspace{-5mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|limit| (optional) & int  & Die maximale Anzahl an Aufträgen (default: 20), die zurückgegeben wird.\\
    \hline
    \verb|page| (optional) & int & Die angefragte Seite (default: 1). \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|get_tasks_tosign(): Response|
    \item[Beschreibung] Die Methode gibt alle aktiven Aufträge, die vom anfragenden Nutzer signiert werden können, zurück.
    \item[Nötige Zugriffsrechte] Nutzer
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
      \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
      \textit{Bad Request}: \verb|limit|, \verb|page| keine natürlichen Zahlen.
    \item[Rückgabe] Statuscode, JSON-String mit Task-Objekten im \nameref{sec:task}{-Schema}
  \end{description}

\end{routesbox}

\begin{routesbox}{\fbox{GET} \textbf{/api/v1/tasks?type=ARCHIVED}\textcolor{darkgray}{\textbf{?\textit{limit=20}\&\textit{page=1}}}}

  \quad
  \vspace{-5mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|limit| (optional) & int  & Die maximale Anzahl an Aufträgen (default: 20), die zurückgegeben wird.\\
    \hline
    \verb|page| (optional) & int & Die angefragte Seite (default: 1). \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|get_tasks_archived(): Response|
    \item[Beschreibung] Die Methode gibt alle fertiggestellten Aufträge, die vom anfragenden Nutzer erstellt oder signiert wurden, zurück.
    \item[Nötige Zugriffsrechte] Nutzer
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
      \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
      \textit{Bad Request}: \verb|limit|, \verb|page| keine natürlichen Zahlen.
    \item[Rückgabe] Statuscode, JSON-String mit Task-Objekten im \nameref{sec:task}{-Schema}
  \end{description}

\end{routesbox}

\begin{routesbox}{\fbox{POST} \textbf{/api/v1/tasks}}
  \begin{minipage}{\textwidth}
  Der JSON-String, der dieser Route übergeben wird, hat folgendes Format:

  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Attribut & Typ & Beschreibung \\
    \hline
    \verb|endpoint_id| & int & Die eindeutige Identifikationsnummer des zugehörigen Endpunkts. \\
    \hline
    \verb|name| & String & Name des Auftrags. \\
    \hline
    \verb|fields| & Array of \nameref{sec:field}{} & Eine Menge an Feldern, die aus Typ, Name und Wert bestehen. \\
    \hline
    \verb|accesses| & Array of \nameref{sec:access}{} & Eine Menge an Gruppen/Nutzern, die einen Auftrag zu diesem Endpunkt signieren müssen. \\
    \hline
    \verb|insert_before| & boolean & Gibt an, wann die vom Auftragsersteller spezifizierten Signaturen signieren müssen. \newline \verb|True|: am Anfang \newline \verb|False|: am Ende \\
    \hline
  \end{longtabu*}
  \end{minipage}
  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|create_task(): Response|
    \item[Beschreibung] Die Methode erzeugt zu den in einem JSON-String übergebenen Attributen eines Auftrags einen neuen Auftrag und fügt diesen zu der Datenbank hinzu.
    \item[Nötige Zugriffsrechte] Nutzer
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
    \textit{Bad Request}: Übergebene Attribute sind ungültig (falscher Datentyp, zu wenig Daten, referenzierter Endpunkt, Felder, Nutzer/Gruppen existieren nicht). Der Nutzer fügt benötigte Signaturen hinzu, obwohl der Endpunkt keine weiteren Signaturen erlaubt.
    \item[Rückgabe] Statuscode, JSON-String mit dem erstellten Auftrag im \nameref{sec:task}{-Schema} und der URL des erstellten Auftrags
  \end{description}

\end{routesbox}

\begin{routesbox}{\fbox{PUT} \textbf{/api/v1/tasks/\textit{<task_id>}/status}}
  \begin{minipage}{\textwidth}
    \quad
    \vspace{-5mm}
    \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
      Parameter & Typ & Beschreibung \\
      \hline
      \verb|task_id| & int & Die eindeutige Identifikationsnummer eines Auftrags. \\
      \hline
    \end{longtabu*}

    In dem JSON-String der Anfrage wird folgendes Attribut übergeben:
  \end{minipage}

  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Attribut & Typ & Beschreibung \\
    \hline
    \verb|event| & String & Gibt an, ob der anfragende Nutzer den Auftrag eingesehen, heruntergeladen, signiert oder abgelehnt hat. \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|update_task(task_id: int): Response|
    \item[Beschreibung] Je nach \verb|event| wird der Status des Auftrags geändert. Ist bei dem Auftrag eine Signatur erst nach Download möglich, wird vor der Signatur zuerst geprüft, ob der Status des Auftrags \verb|downloaded| ist. Bei einer erfolgreichen Signatur werden ein Zeitstempel und der signierende Nutzer zum jeweiligen Eintrag in der \verb|Access|-Tabelle hinzugefügt. Beim sequentiellen Modus wird der Nutzer oder die Gruppe benachrichtigt, die als nächstes signieren muss. Sind alle benötigten Signaturen vorhanden, wird der Auftrag als fertiggestellt gekennzeichnet und der Ersteller des Auftrags wird benachrichtigt. Wird der Auftrag abgelehnt, wird er als solcher gekennzeichnet und der Ersteller des Auftrags wird benachrichtigt.
    \item[Nötige Zugriffsrechte] Nutzer, dessen Signatur bei diesem Auftrag als nächstes benötigt wird oder der einer Gruppe angehört, deren Signatur als nächstes benötigt wird
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
      \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
      \textit{Not Found}: Kein aktiver Auftrag zur gegebenen Identifikationsnummer. \\
      \textit{Bad Request}: \verb|event| ist keine valide Nutzeraktion.
    \item[Rückgabe] Statuscode
  \end{description}

\end{routesbox}

\begin{routesbox}{\fbox{DELETE} \textbf{/api/v1/tasks/\textit{<task_id>}}}
  \begin{minipage}{\textwidth}
    \quad
    \vspace{-5mm}
    \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
      Parameter & Typ & Beschreibung \\
      \hline
      \verb|task_id| & int & Die eindeutige Identifikationsnummer eines Auftrags. \\
      \hline
    \end{longtabu*}

    \begin{description}[leftmargin=0pt]
      \item[Methodenaufruf] \verb|delete_task(task_id: int): Response|
      \item[Beschreibung] Ist der Auftrag mit der Identifikationsnummer \verb |task_id| noch nicht fertiggestellt, wird er als zurückgezogen gekennzeichnet und kann nicht mehr signiert werden. Ist der Auftrag bereits fertiggestellt, wird er als gelöscht gekennzeichnet und kann nicht mehr eingesehen werden.
      \item[Nötige Zugriffsrechte] Nutzer, der den Auftrag erstellt hat
    \end{description}
  \end{minipage}
  \begin{description}[leftmargin=0pt]
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt   nicht die nötigen Zugriffsrechte. \\
    \textit{Not Found}: Kein Auftrag zur gegebenen Identifikationsnummer. \\
    \textit{Bad Request}: Der Auftrag ist fertiggestellt und nicht löschbar.
    \item[Rückgabe] Statuscode
  \end{description}

\end{routesbox}

\begin{routesbox}{\fbox{GET} \textbf{/api/v1/tasks/\textit{<task_id>}/files}}
  \begin{minipage}{\textwidth}
    \quad
    \vspace{-5mm}
    \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
      Parameter & Typ & Beschreibung \\
      \hline
      \verb|task_id| & int & Die Identifikationsnummer des zu den angefragten Dateien zugehörigen Auftrags.\\
      \hline
    \end{longtabu*}

    In dem JSON-String der Anfrage wird folgendes Attribut übergeben:

    \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
      Attribut & Typ & Beschreibung \\
      \hline
      \verb|file_uuid| & int & Die eindeutige Identifikationsnummer der angefragten Datei.\\
      \hline
    \end{longtabu*}
  \end{minipage}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|get_file_url(task_id: int, file_uuid: int): Response|
    \item[Beschreibung] Die Methode gibt einen zeitlich befristeten Download-Link zu einer Datei mit der Identifikationsnummer \verb|file_uuid| von einem Auftrag mit der Identifikationsnummer \verb|task_id| zurück.
    \item[Nötige Zugriffsrechte] Nutzer, der an dem Auftrag beteiligt ist
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
    \textit{Not Found}: Kein Auftrag zur gegebenen Identifikationsnummer. \\
    \textit{Bad Request}: Die gegebene Identifikationsnummer existiert nicht oder gehört nicht zum Auftrag.
    \item[Rückgabe] Statuscode, JSON-String mit Download-Link in folgendem Format:
  \end{description}
  \vspace{-4mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Attribut & Typ & Beschreibung \\
    \hline
    \verb|download_link| & String & Download-Link zur angefragten Datei.\\
    \hline
  \end{longtabu*}

\end{routesbox}

\begin{routesbox}{\fbox{GET} \textbf{/api/v1/tasks/\textit{<task_id>}/comments}\textcolor{darkgray}{\textbf{?\textit{limit=20}\&\textit{page=1}}}}

  \quad
  \vspace{-5mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline 
    \verb|task_id| & int & Die Identifikationsnummer eines Auftrags. \\
    \hline
    \verb|limit| (optional) & int & Die maximale Anzahl an Nachrichten (default: 20), die zurückgegeben wird.\\
    \hline
    \verb|page| (optional) & int & Die angefragte Seite (default: 1). \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|get_comments(task_id: int): Response|
    \item[Beschreibung] Die Methode gibt alle nicht gelöschten Kommentare des Auftrags mit der Identifikationsnummer \verb|task_id| zurück.
    \item[Nötige Zugriffsrechte] Nutzer, der an dem Auftrag beteiligt ist
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
    \textit{Not Found}: Kein Auftrag zur gegebenen Identifikationsnummer. \\
    \textit{Bad Request}: \verb|limit|, \verb|page| keine natürlichen Zahlen.
    \item[Rückgabe] Statuscode, JSON-String mit Comment-Objekten im \nameref{sec:comment}{-Schema}
  \end{description}

\end{routesbox}
\vspace{-1mm}
\begin{routesbox}{\fbox{GET} \textbf{/api/v1/tasks/\textit{<task_id>}/comments/\textit{<comment_id>}}}

  \quad
  \vspace{-5mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|task_id| & int & Die Identifikationsnummer eines Auftrags. \\
    \hline
    \verb|comment_id| & int & Die Identifikationsnummer eines Kommentars. \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|get_comment(task_id: int, comment_id: int): Response|
    \item[Beschreibung] Die Methode gibt zu einem Auftrag mit der Identifikationsnummer \verb|task_id| den Kommentar mit der Identifikationsnummer \verb|comment_id| zurück.
    \item[Nötige Zugriffsrechte] Nutzer, der an dem Auftrag beteiligt ist
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
    \textit{Not Found}: Kein Auftrag oder kein Kommentar zur gegebenen Identifikationsnummer. \\
    \textit{Bad Request}: Kommentar mit der gegebenen Identifikationsnummer gehört nicht zum Auftrag.
    \item[Rückgabe] Statuscode, JSON-String mit einem Comment-Objekt im \nameref{sec:comment}{-Schema}
  \end{description}

\end{routesbox}

\begin{routesbox}{\fbox{POST} \textbf{/api/v1/tasks/\textit{<task_id>}/comments}}
  \begin{minipage}{\textwidth}
    \quad
    \vspace{-5mm}
    \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
      Parameter & Typ & Beschreibung \\
      \hline
      \verb|task_id| & int & Identifikationsnummer eines Auftrags. \\
      \hline
    \end{longtabu*}

    In dem JSON-String der Anfrage wird folgendes Attribut übergeben:

    \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
      Parameter & Typ & Beschreibung \\
      \hline
      \verb|message| & String & Der Kommentartext. \\
      \hline
    \end{longtabu*}
  \end{minipage}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|create_comment(task_id: int): Response|
    \item[Beschreibung] Die Methode erstellt einen neuen Kommentar zu einem Auftrag mit der Identifikationsnummer \verb|task_id| und fügt diesen zur Datenbank hinzu.
    \item[Nötige Zugriffsrechte] Nutzer, der an dem Auftrag beteiligt ist
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
    \textit{Not Found}: Kein aktiver Auftrag zur gegebenen Identifikationsnummer. \\
    \textit{Bad Request}: Kommentar ist länger als 100 Zeichen.
    \item[Rückgabe] Statuscode, JSON-String mit dem erstellten Kommentar im \nameref{sec:comment}{-Schema} und der URL des erstellten Kommentars.
  \end{description}

\end{routesbox}

\subsubsection{user_handler}
\begin{routesbox}{\fbox{GET} \textbf{/api/v1/users/search}}
  \begin{minipage}{\textwidth}
    In dem JSON-String der Anfrage wird folgendes Attribut übergeben:

    \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
      Attribut & Typ & Beschreibung \\
      \hline
      \verb|search_term| & String & Der String, nach dem in der \verb|Users|-Tabelle gesucht wird. \\
      \hline
    \end{longtabu*}

    \begin{description}[leftmargin=0pt]
      \item[Methodenaufruf] \verb|search_user(): Response|
      \item[Beschreibung] Die Methode gibt alle Nutzer, deren Namen, Nutzernamen oder Identifikationsnummern den String \verb|search_term| beinhalten, zurück.
    \end{description} 
  \end{minipage}
  \begin{description}[leftmargin=0pt]
    \item[Nötige Zugriffsrechte] Nutzer
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
    \textit{Not Found}: Kein Nutzer, dessen Name, Nutzername oder Identifikationsnummer den String \verb|search_term| beinhaltet.
    \item[Rückgabe] Statuscode, JSON-String mit User-Objekten im \nameref{sec:user}{-Schema}
  \end{description}

\end{routesbox}

\subsubsection{message_handler}
\begin{routesbox}{\fbox{GET} \textbf{/api/v1/messages}\textcolor{darkgray}{\textbf{?\textit{limit=20}\&\textit{page=1}}}}
  \quad
  \vspace{-5mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|limit| (optional) & int & Die maximale Anzahl an Nachrichten (default: 20), die zurückgegeben wird.\\
    \hline
    \verb|page| (optional) & int & Die angefragte Seite (default: 1). \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|get_messages(): Response|
    \item[Beschreibung] Die Methode gibt alle ungelöschten Nachrichten an den anfragenden Nutzer zurück.
    \item[Nötige Zugriffsrechte] Nutzer
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
    \textit{Bad Request}: \verb|limit|, \verb|page| keine natürlichen Zahlen.
    \item[Rückgabe] Statuscode, JSON-String mit Message-Objekten im \nameref{sec:message}{-Schema}
  \end{description}

\end{routesbox}

\begin{routesbox}{\fbox{GET} \textbf{/api/v1/messages/\textit{<message_id>}}}
  \begin{minipage}{\textwidth}
    \quad
    \vspace{-5mm}
    \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
      Parameter & Typ & Beschreibung \\
      \hline
      \verb|message_id| & int & Die Identifkationsnummer einer Nachricht. \\
      \hline
    \end{longtabu*}

    \begin{description}[leftmargin=0pt]
      \item[Methodenaufruf] \verb|get_messages(): Response|
      \item[Beschreibung] Die Methode gibt die Nachricht mit der Identifikationsnummer \verb|message_id| an den anfragenden Nutzer zurück.
      \item[Nötige Zugriffsrechte] Nutzer, der die Benachrichtigung mit der \verb|message_id| empfangen hat.
    \end{description}
  \end{minipage}
  \begin{description}[leftmargin=0pt]
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
    \textit{Not Found}: Keine ungelöschte Benachrichtigung zur gegebenen \verb|message_id|.
    \item[Rückgabe] Statuscode, JSON-String mit einem Message-Objekt im \nameref{sec:message}{-Schema}
  \end{description}

\end{routesbox}

\vspace{-1mm}
\begin{routesbox}{\fbox{PUT} \textbf{/api/v1/messages/\textit{<message_id>}/status}}

  \quad
  \vspace{-5mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Parameter & Typ & Beschreibung \\
    \hline
    \verb|message_id| & int & Die Identifikationsnummer einer Nachricht. \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|update_read_status(message_id: int): Response|
    \item[Beschreibung] Die Methode setzt den Status der Nachricht mit der Identifikationsnummer \verb|message_id| auf gelesen.
    \item[Nötige Zugriffsrechte]  Nutzer, der die Benachrichtigung mit der \verb|message_id| empfangen hat
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
    \textit{Not Found}: Keine ungelöschte Benachrichtigung zur gegebenen \verb|message_id|.
    \item[Rückgabe] Statuscode
  \end{description}

\end{routesbox}

\subsubsection{preference_handler}
\vspace{-1mm}
\begin{routesbox}{\fbox{GET} \textbf{/api/v1/preferences/languages}}
  \quad
  \vspace{-5mm}
  \begin{description} [leftmargin=0pt]
    \item[Methodenaufruf] \verb|get_language(): Response|
    \item[Beschreibung] Die Methode gibt die eingestellte Sprache des anfragenden Nutzers zurück.
    \item[Nötige Zugriffsrechte] Nutzer 
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. 
    \item[Rückgabe] Statuscode, JSON-String in folgendem Format:
\end{description}
\vspace{-4mm}
\begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
  Attribut & Typ & Beschreibung \\
  \hline
  \verb|language| & String & Die eingestellte Sprache des Nutzer. \\
  \hline
\end{longtabu*}
\end{routesbox}

\begin{routesbox}{\fbox{POST} \textbf{/api/v1/preferences/languages}}
In dem JSON-String der Anfrage wird folgendes Attribut übergeben:
\begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
  Attribut & Typ & Beschreibung \\    
  \hline
  \verb|language| & String & Sprache \\
  \hline
\end{longtabu*}

\begin{description} [leftmargin=0pt]
  \item[Methodenaufruf] \verb|set_language(): Response|
  \item[Beschreibung] Die Methode setzt die eingestellte Sprache des anfragenden Nutzers auf die Sprache \verb|language|.
  \item[Nötige Zugriffsrechte] Nutzer 
  \item[Mögliche Fehler] \hfill \\
  \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
  \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
  \textit{Bad Request}: \verb|language| ist keine unterstützte Sprache. 
  \item[Rückgabe] Statuscode
\end{description}
\end{routesbox}


\begin{routesbox}{\fbox{GET} \textbf{/api/v1/preferences/notifications}}
  \quad
  \vspace{-5mm}
  \begin{description} [leftmargin=0pt]
    \item[Methodenaufruf] \verb|get_email_notification_status(): Response|
    \item[Beschreibung] Die Methode gibt die Einstellung der E-Mail-Benachrichtigung des anfragenden Nutzers zurück.
    \item[Nötige Zugriffsrechte] Nutzer 
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. 
    \item[Rückgabe] Statuscode, JSON-String in folgendem Format:
  \end{description}
  \vspace{-4mm}
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Attribut & Typ & Beschreibung \\    
    \hline
    \verb|notification_status| & boolean & \verb|True|: E-Mail-Benachrichtigungen an \newline \verb|False|: E-Mail-Benachrichtigungen aus \\
    \hline
  \end{longtabu*}
\end{routesbox}

\newpage
\begin{routesbox}{\fbox{POST} \textbf{/api/v1/preferences/notifications}}
In dem JSON-String der Anfrage wird folgendes Attribut übergeben:

\begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
  Attribut & Typ & Beschreibung \\
  \hline
  \verb|notification_status| & boolean & \verb|True|: E-Mail-Benachrichtigungen an \newline \verb|False|: E-Mail-Benachrichtigungen aus \\
  \hline
\end{longtabu*}

\begin{description} [leftmargin=0pt]
  \item[Methodenaufruf] \verb|set_email_notification_status(): Response|
  \item[Beschreibung] Die Methode setzt die Einstellung der Email-Benachrichtigung des anfragenden Nutzers auf den Wert von \verb|notification_status|.
  \item[Nötige Zugriffsrechte] Nutzer 
  \item[Mögliche Fehler] \hfill \\
  \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
  \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
  \textit{Bad Request}: \verb|notification_status| hat den falschen Datentyp.
  \item[Rückgabe] Statuscode
\end{description}
\end{routesbox}


\subsubsection{group_handler}
\begin{routesbox}{\fbox{GET} \textbf{/api/v1/groups/search}}
  In dem JSON-String der Anfrage wird folgendes Attribut übergeben:
  
  \begin{longtabu*} to \textwidth {X[2,l] X[1,l] X[4,l]}
    Attribut & Typ & Beschreibung \\
    \hline
    \verb|search_term| & String & Der String, nach dem in der \verb|Groups|-Tabelle gesucht wird. \\
    \hline
  \end{longtabu*}

  \begin{description}[leftmargin=0pt]
    \item[Methodenaufruf] \verb|search_group(): Response|
    \item[Beschreibung] Die Methode gibt alle Gruppen, deren Namen den String \verb|search_term| beinhalten, zurück.
    \item[Nötige Zugriffsrechte] Nutzer
    \item[Mögliche Fehler] \hfill \\
    \textit{Unauthorized}: Der Anfragende ist nicht angemeldet. \\
    \textit{Forbidden}: Der Anfragende besitzt nicht die nötigen Zugriffsrechte. \\
    \textit{Not Found}: Keine Gruppe, deren Name den String \verb|search_term| beinhaltet.
    \item[Rückgabe] Statuscode, JSON-String mit Group-Objekten im \nameref{sec:group}{-Schema}
  \end{description}

\end{routesbox}